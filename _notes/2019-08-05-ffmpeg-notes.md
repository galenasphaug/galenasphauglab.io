---
layout: note
title: FFMpeg
detailed-title: FFMpeg Notes
author: Galen Asphaug
---

FFMpeg is a really cool command line utility for converting videos

	ffmpeg -i input [options] output

#### Codec options

`-acodec` (`-c:a`)

* `aac`
  
`-vcodec` (`-c:v`)

* `h264`
* `libx265`
* `copy` (skip encoding, i.e for rotating or muting video)

#### Common Options:
	
`-ss [start timestamp]`
`-t [duration]`
`-an` (no audio)
`-vf [filter]`, `-filter:v`

#### Using CRF with H264/265 encoding:
`-crf` (constant rate factor)
`-crf 24` (lower quality)
`-crf 15` (higher quality)

#### Cut frame x/y in half to save storage
	
	-vf scale=iw/2:ih/2

* 1080p becomes 960x540
* 720p becomes 640x360

#### Scale video

    -vf scale=1280:720

<https://trac.ffmpeg.org/wiki/Scaling>:

"If you need to simply resize your video to a specific size (e.g 320 x 240), you can use the scale filter in its most basic form:

	ffmpeg -i input.avi -vf scale=320:240 output.avi
"

#### Speed up video for time lapse

Either drop every Xth frame, or change the frame rate of the video, or both. Dropping frames (changing PTS values) will **NOT** change the audio, so either mute it with `-an`, or speed it up to match the video with the `atempo` filter.

##### To double the speed of the video while keeping the same framerate, you can use:

    ffmpeg -i input.mkv -filter:v "setpts=0.5*PTS" -an output.mkv
(`setpts=0.5*PTS` will drop every other frame)

##### Drop every other frame and also double frame rate from 1080p30 video (4x speed, 60fps)

	ffmpeg -i GOPRXXXX.MP4 -r 60 -filter:v setpts=0.25*PTS -an timelapse.mp4

<https://trac.ffmpeg.org/wiki/How%20to%20speed%20up%20/%20slow%20down%20a%20video>

#### Generate time lapse from sets of images
	
With a directory structure like this on a GoPro:
	
	119GOPRO/
	120GOPRO/
	121GOPRO/
	122GOPRO/
	...

Use this to stitch them together, set fps to 60, and set the CRF to 30:
	
	ffmpeg -i "%*GOPRO/%*.JPG" -r 60 -crf 30 output.mkv

#### Stabilize video (or time lapse!)

* Easy fast version:

		ffmpeg -i input.mov -vf deshake output.mov

* Harder (better) *slower* **stronger** version:

		ffmpeg -i timelapse.mp4 -vf vidstabdetect=stepsize=32:shakiness=10:accuracy=10:result=transform_vectors.trf -f null -
	
		ffmpeg -i input.mp4 -vf vidstabtransform=input=transform_vectors.trf:zoom=0:smoothing=10,unsharp=5:5:0.8:3:3:0.4 -vcodec libx264 -tune film -an stabilized.mp4

<https://www.imakewebsites.ca/posts/2018/02/17/stabilizing-gopro-video-with-ffmpeg-and-vid.stab/>

#### Video compression for GoPro 1080p30 and 720p60

    ffmpeg -i GOPRXXXX.MP4 -crf 35 -vf "scale=iw/2:ih/2" -filter:v fps=fps=24 -vcodec libx265 compressed.mp4

#### Hardware encoding

	ffmpeg -i input.mkv -vaapi_device /dev/dri/renderD128 -vf "format=nv12,hwupload" -c:v h264_vaapi output.mkv

#### Motion interpolation

	ffmpeg -i input.mkv -filter:v "minterpolate='mi_mode=mci:mc_mode=aobmc:vsbmc=1:fps=240'" output.mkv
	
...and then change the frame rate to 30fps (30/240 = 1/8 speed)

	ffmpeg -i output.mkv -r 30 slow.mkv

#### Stack videos vertically or horizontally

	ffmpeg -i a.mp4 -i b.mp4 -filter_complex vstack merged.mp4
	ffmpeg -i a.mp4 -i b.mp4 -filter_complex hstack merged.mp4

