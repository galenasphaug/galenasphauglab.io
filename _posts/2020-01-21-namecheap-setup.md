---
layout: post
title: NameCheap and GitLab Pages
detailed-title: Using a NameCheap Domain With GitLab Pages
author: Galen Asphaug
---

It took me a while to figure out how to configure GitLab to work with my NameCheap domain. There are a lot of conflicting documents that GitLab has published, making it very confusing to configure properly. If you want to host a page at your root domain (*`example.com`*), make sure that you are hosting your GitLab page for a user (*`username.gitlab.io`*), and not a project (*`username.gitlab.io/project/`*). If you want to host at a subdomain, you should use a project page. GitHub has a [doc](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html#gitlab-pages-default-domain-names) explaining the difference between user and project pages.

After you have your domain name and GitLab page setup (and you have triggered a GitLab pipeline to build and publish your page), go to your project, and navigate to `Settings > Pages`. Make sure the default user or project domain is listed. Click `New Domain` and type in *`example.com`* or *`subdomain.example.com`* depending on if you have a user or project page. Enable automatic certificate management using Let's Encrypt, and click `Create New Domain`.

Now, you need to verify that the domain name you entered belongs to you. Go to your NameCheap domain's "Advanced DNS" settings, and add a new `TXT` DNS record. Set the `host` to `_gitlab-pages-verification-code` (**DON'T include the trailing "`.example.com`" for NameCheap, it is already appended!**) Set the value to `gitlab-pages-verification-code=your_verification_code`.

If you are using your root domain name, add a DNS `A` record to point to GitLab Page's IP (currently `35.185.44.232`. This doesn't change often, but has changed twice since 2017). The configuration for the domain on GitLab Pages will tell you to enter a `CNAME` record instead, but that is [incorrect](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/#3-set-up-dns-records)! Host = `@` (`@` = your root domain), Value = `35.185.44.232`.

If you are using a subdomain, add a DNS `CNAME` record. Set the `host` to `subdomain` (**NOT** `subdomain.example.com`, NameCheap automatically appends the domain name), and the `value` to `username.gitlab.io.` (with the trailing period).

You can host both a root and subdomain at the same time, just add both records. For more information on DNS records, visit [NameCheap's documentation](https://www.namecheap.com/support/knowledgebase/article.aspx/579/2237/which-record-type-option-should-i-choose-for-the-information-im-about-to-enter/).

Now apply your new DNS settings, and wait a few minutes for your changes to propagate through the DNS system (this can take some time, but it usually happens in under a minute). After some time has passed, click the "Retry verification" box on your pages settings. You should get a message saying "Successfully verified domain ownership". If not, wait a little longer and try again.

If you are having trouble verifying your domain, you can use a program called `dig` to check your domains DNS entries:

To check that the domain name's DNS configuration has been sucessfully updated:

	dig example.com

OR:

	dig subdomain.example.com

...And look for:
	
	;; ANSWER SECTION:
	example.com.    1234    IN    A    35.185.44.232

OR:

	;; ANSWER SECTION:
	subdomain.example.com.    1234    IN    CNAME    username.gitlab.io.

To check that the verification code has been published:

	dig _gitlab-pages-verification-code.example.com TXT

Look for:

	;; ANSWER SECTION:
	_gitlab-pages-verification-code.example.com.    1234    IN    TXT    "gitlab-pages-verification-code=XXXXXXXX"

Once you successfully verify your domain, you should see a message that GitLab is working on your SSL certificate for HTTPS. This can take a few days, as GitLab has a queue of many certificates they are generating.

The final step is to trigger another GitLab CI pipeline build for your project. This was the missing piece of the puzzle for me, for an embarrassingly long time. Go to your project's page, and navigate to `CI / CD > Pipelines`, and click `Run Pipeline`. After the build finishes, your page should be live on your domain! This can take some time (around 30 minutes) for your first deployment.

Troubleshooting
===============

I hate to say it, but the best method of troubleshooting for me was to delete the entire project (making sure to change the slug so you can reuse the same project name, say, `username.gitlab.io-deleted`), disable the pages, and create a new project with the same name. `git push` all your old files to the new project, and repeat the steps above.
