---
layout: post
title: GitLab Pages with Jekyll
detailed-title: Using GitLab Pages with Jekyll
author: Galen Asphaug
---

Jekyll is a static website builder that makes it simple to create basic websites, like blogs, portfolios, etc. It makes it simple to create modern websites from plain text. For more information on Jekyll, visit <https://jekyllrb.com>. To learn more about starting a website from scratch, visit <https://jekyllrb.com/docs/step-by-step/01-setup/>.

## Installation

To get started, make sure you have a Ruby development environment installed on your machine, the package you need to install is usually called `ruby-devel`. Next is to install the `jekyll` and `bundler` ruby gems. `bundler` ensures that you are using the right version for your project.

{% highlight sh %}
gem install jekyll bundler
{% endhighlight %}

## Setup

Setting up a static website with GitLab using Jekyll is rather simple, and can be done in a few hours. Assuming you already have an account with GitLab, the first step is to create a new repository with the title `<username>.gitlab.io`. You can also create project pages that will get hosted at `<username>.gitlab.io/projectname`. For more info on the different types of pages, visit [here](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_one.html). Clone the repository to a folder on your computer, create a `Gemfile`, and add the following line. This will tell bundler what version of Jekyll to use for your website:

{% highlight ruby %}
gem "jekyll"
{% endhighlight %}
	
Now run `bundle` to install Jekyll for your project. Using the command `bundle exec` prefixed by any Jekyll command will ensure you are using the same version of Jekyll.

Now that Jekyll is setup, create a `_layouts` folder. Add a `default.html` file inside the `_layouts` folder and add the following lines:

{% highlight html %}
{% raw %}
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>{{ page.title }}</title>
	</head>
	<body>
		{% include navigation.html %}
		{{ content }}
	</html>
</body>
{% endraw %}
{% endhighlight %}

{% raw %}
This looks very similar to plain HTML, except there are a few changes. The `{{` and `}}` are tags indicating "Liquid" code. For more information on Liquid, visit [here](https://shopify.github.io/liquid/basics/introduction/). This file will be used as a wrapper for any pages using the `default` layout.
{% endraw %}

Now create your first page. Go to the root of your website and create a new Markdown file, `index.md`. This will be processed by Jekyll and exported as an HTML file. For more information om Markdown, take a look at the creator's [documentation](https://daringfireball.net/projects/markdown/syntax). There is also a handy [cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet). Insert the following lines to the file:

{% highlight markdown %}
---
layout: default
title: Home
---

# Hello World
	
This is my first Jekyll website.
{% endhighlight %}

This looks similar to Markdown, except for the `---`. This is called "front matter" and tells the Jekyll interpreter to process this page. Front matter is a snippet of YAML that resides at the top of the file between two triple-dashed lines, and is used to set variables for the page, such as `title: Home`, or the type of page layout (default, blog post, etc...). Variables can be anything you want, for instance, `number-of-fingers: 5`, or `number-of-dogs: plenty`.

Now lets create your About page. Create a new Markdown file called `about.md` in the root directory and insert the following:

{% highlight markdown %}
---
layout: default
title: About
---

# About page

I'm really cool
{% endhighlight %}

Notice how you don't have to include any HTML to make this work, it's just plain text. Now let's work on the navigation.

Create the data for navigation at `_data/navigation.yml`. Include the following lines:

{% highlight yml %}
- name: Home
  link: /
- name: About
  link: /about.html
{% endhighlight %}

Jekyll makes this data available to you at `site.data.navigation`. Now let's create an HTML file for the navigation. Create a new folder called `_includes` and put a `navigation.html` file in there. The `_includes` file contains snippets of HTML that you can include anywhere in your pages. Include the following:

{% highlight html %}
{% raw %}
<nav>
  {% for item in site.data.navigation %}
    <a href="{{ item.link }}"
	  {% if page.url == item.link %}
	    class="current"
	  {% endif %}>
      {{ item.name }}
    </a>
  {% endfor %}
</nav>
{% endraw %}
{% endhighlight %}

Now it's getting interesting. The Liquid code for the navigation snippet will iterate over all the `item`s inside `_data/navigation.yml` and create a link with the `item.link` and `item.name` variables for each one. Jekyll will also check if the `page.url` (current URL) is equal to the `item.url`, and change the `class` of the HTML element to equal `current`. This will come in handy for highlighting the current page. The navigation will appear on any pages using the `default` layout, because we included the `default.html` file in the `<body>` of the page.

To create the styling for your website, we will use a language similar to CSS, called Sass. To learn more about Sass, visit <https://sass-lang.com/guide>. Create a file at `assets/css/styles.scss` and insert the following:

{% highlight css %}
---
---
@import "main";
{% endhighlight %}

Again, the empty front matter tells Jekyll to process this file, and the `@import "main";` line tells Sass to look for a file called `main.scss` in the Sass directory (`_sass/`). Create said file at `_sass/main.scss`, and insert the following:

{% highlight css %}
.current {
  color: red;
}
{% endhighlight %}

Remember how we made the HTML `class` for the active page set to `current` in `navigation.html`? This will color that link red to make it more visible. Jekyll will process this file (since it has front matter) and export it as `assets/css/styles.css`. Let's include that processed stylesheet into our `default.html` layout, so each page has the same CSS. Go back to `_layouts/default.html` and edit it to look like this:

{% highlight html %}
{% raw %}
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>{{ page.title }}</title>
		<link rel="stylesheet" href="/assets/css/styles.css">
	</head>
	<body>
		{% include navigation.html %}
		{{ content }}
	</html>
</body>
{% endraw %}
{% endhighlight %}

## Testing

It's about time to view your hard work. Go back to the root folder and run `bundle exec jekyll serve`. This will compile the entire website, and host it locally at <http://localhost:4000/>. Click around and make sure the navigation works. If it doesn't, then follow a better tutorial than this one.

## Blogging

Jekyll has many features built in to make blogging very simple to use. To get started, make a folder for all your posts called, you guessed it, `_posts`. This will contain Markdown files like `index.md` and `about.md`, but are named slightly different. They begin with the published date in YYYY-MM-DD format, followed by the title and extension. Create your first post at `_posts/YYYY-MM-DD-first-post.md`, with the following content:

{% highlight markdown %}
---
layout: post
---

Hello, this is my first Jekyll post.
{% endhighlight %}

Notice that we don't have a `post` layout defined in `_layouts` yet. Let's create that now. `_layouts/post.html`:

{% highlight html %}
{% raw %}
---
layout: default
---
<h1>{{ page.title }}</h1>
<p>{{ page.date | date_to_string }}</p>

{{ content }}
{% endraw %}
{% endhighlight %}

This will take the `page.title`, `page.date` and the post's `content` and format it nicely. Note that the `post` layout inherits all other attributes from the `default` layout defined in it's front matter. 

Now we can include a link to the blog in the site navigation. Add an entry for the blog page in `_data/navigation.yml`:

{% highlight yml %}
- name: Home
  link: /
- name: About
  link: /about.html
- name: Blog
  link: /blog.html
{% endhighlight %}

And next, create a file for the blog index, `blog.html` in the root directory. Add the following:

{% highlight html %}
{% raw %}
---
layout: default
title: Blog
---
<h1>Latest Posts</h1>

<ul>
  {% for post in site.posts %}
    <li>
      <h2><a href="{{ post.url }}">{{ post.title }}</a></h2>
      <p>{{ post.excerpt }}</p>
    </li>
  {% endfor %}
</ul>
{% endraw %}
{% endhighlight %}

This does a similar operation to the Liquid code in `navigation.html`, it iterates over each `post` in `site.posts`, creating a list of titles and exerpts. `post.exerpt` by default includes the first paragraph of the post. Go agead and add a few more posts, and then compile and view the site with `bundle exec jekyll serve`. Any change you make will be updated immediately, so all you need to do is save and reload while `bundle exec jekyll serve` is running.

## Collections

Collections are similar to posts, except the content doesn't have to be grouped by date. Collections can be used for a list of biographies, list of notes, etc. To create a new collection, you need to tell Jekyll about it. Create a file at the root of the website titled `_config.yml`. This is the main site configuration for jekyll. To create a collection of notes, insert the following:

{% highlight yml %}
collections:
  notes:
    output: true
{% endhighlight %}

Note that any change to `_config.yml` will require you to restart `bundle exec jekyll serve`. Items in a collection will go under `_<collection_name>/`. In this case, `_notes`. `output: true` tells Jekyll to process the items in the collection and produce pages for each one. Create a Markdown file for each note you want to take, making sure to include the front matter:

{% highlight markdown %}
{% raw %}
---
title: Note
---

# {% page.title %}
{% endraw %}
{% endhighlight %}

Feel free to add as many items to the collection as you like. Now, lets create a link in the navigation for your notes:

{% highlight yml %}
- name: Home
  link: /
- name: About
  link: /about.html
- name: Blog
  link: /blog.html
- name: Notes
  link: /notes.html
{% endhighlight %}

Tada! You now have a fully functional website with the ability to easily create blog entries and notes.

## GitLab Hosting

To host on GitLab, you need to configure GitLab CI. Create a new file in the root of your website titled `.gitlab-ci.yml`. Add the following:

{% highlight yml %}
pages:
  script:
    - bundle install
    - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
{% endhighlight %}

This tells GitLab to start a job called `pages`, that in turn executes the script that tells `bundler` to install the dependencies in the `Gemfile`, and then use Jekyll to build to the `public/` directory using the `-d` flag. This is where GitLab searches for exported files to host. For more information on using GitLab CI with Jekyll, visit <https://docs.gitlab.com/ee/user/project/pages/getting_started_part_four.htm>

Next step, use `git` to add, commit, and push all your files to GitLab's servers. Once pushed, GitLab will automatically start building and hosting your webpage in a matter of minutes at `<username>.gitlab.io`. To view the build status, visit the progect page, and navigate to `CI / CD > Jobs`.

Thanks for reading! Enjoy your new website.
